/*
 *    Copyright 2020 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package main

import (
	"fmt"
	"github.com/djcass44/go-tracer/tracer"
	"github.com/getsentry/sentry-go"
	"github.com/gorilla/mux"
	"github.com/namsral/flag"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/rs/cors"
	log "github.com/sirupsen/logrus"
	"gitlab.dcas.dev/open-source/go-prism/pkg/api"
	"gitlab.dcas.dev/open-source/go-prism/pkg/core"
	"gitlab.dcas.dev/open-source/go-prism/pkg/provider"
	"gitlab.dcas.dev/open-source/go-prism/pkg/provider/memory"
	"gitlab.dcas.dev/open-source/go-prism/pkg/provider/s3"
	"net/http"
	"os"
	"time"
)

func main() {
	log.SetFormatter(&log.JSONFormatter{})
	log.SetOutput(os.Stdout)

	debug := flag.Bool("debug", false, "enables debug logging")
	confPath := flag.String("external-config", "/etc/prism/config.yaml", "path to the config yaml file")

	s3Enabled := flag.Bool("s3-enabled", false, "enables the S3 backend")
	s3Endpoint := flag.String("s3-endpoint", "", "s3 endpoint ('' to use Amazon AWS)")
	s3Region := flag.String("s3-region", "", "s3 region")
	s3Bucket := flag.String("s3-bucket", "", "s3 bucket")
	s3PathStyle := flag.Bool("s3-path-style", false, "enables path style (e.g. s3.amazonaws.com/mybucket/myfile rather than mybucket.s3.amazonaws.com/myfile)")

	flag.Parse()

	if *debug {
		log.SetLevel(log.DebugLevel)
		log.Debug("enabled debug logging")
	} else {
		log.SetLevel(log.InfoLevel)
	}

	err := sentry.Init(sentry.ClientOptions{})
	if err != nil {
		log.WithError(err).Fatal("failed to setup sentry reporter")
		return
	}
	// flush events on exit
	defer sentry.Flush(2 * time.Second)

	// load the config
	c, err := core.ReadConfig(confPath)
	if err != nil {
		sentry.CaptureException(err)
		log.WithError(err).Error("cannot run without valid config")
		return
	}
	log.Infof("restricted headers: %v", c.Cache.RestrictedHeaders)
	core.Config = c
	api.InitInfo()

	// start caches
	var contentCache provider.Provider
	if *s3Enabled {
		conf := &s3.Config{
			Bucket:    *s3Bucket,
			Region:    *s3Region,
			Endpoint:  *s3Endpoint,
			PathStyle: *s3PathStyle,
		}
		contentCache = s3.NewProvider(conf)
	} else {
		contentCache = memory.New(core.Config.Cache.Path)
	}

	// setup proxies
	mvn, err := core.NewProxy("/maven/", c.MavenConfig.Remotes, &c.Cache, contentCache)
	if err != nil {
		log.WithError(err).Panic("failed to initialise Maven proxy")
		return
	}
	npm, err := core.NewProxy("/npm/", c.NodeConfig.Remotes, &c.Cache, contentCache)
	if err != nil {
		log.WithError(err).Panic("failed to initialise NPM proxy")
		return
	}
	apk, err := core.NewProxy("/alpine/", c.AlpineConfig.Remotes, &c.Cache, contentCache)
	if err != nil {
		log.WithError(err).Panic("failed to initialise Alpine proxy")
		return
	}

	// setup the router
	r := mux.NewRouter()

	// serve files on root
	r.PathPrefix(mvn.Path).Handler(tracer.NewHandler(mvn))
	r.PathPrefix(npm.Path).Handler(tracer.NewHandler(npm))
	r.PathPrefix(apk.Path).Handler(tracer.NewHandler(apk))

	// add api paths
	apiHandler := api.NewV1()
	r.HandleFunc("/api/v1/health", apiHandler.GetHealth)
	r.HandleFunc("/api/v1/info", tracer.NewFunc(apiHandler.Info)).Methods(http.MethodGet)

	// misc paths
	r.Handle("/metrics", promhttp.Handler()).Methods(http.MethodGet)
	r.PathPrefix("/").Handler(http.FileServer(http.Dir("/static")))

	// create the handler
	handler := cors.Default().Handler(r)

	addr := fmt.Sprintf(":%d", core.Config.Server.Port)
	log.Infof("starting server on interface %s", addr)
	log.Panic(http.ListenAndServe(addr, handler))
}
