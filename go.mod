module gitlab.dcas.dev/open-source/go-prism

go 1.16

require (
	github.com/aws/aws-sdk-go v1.36.20
	github.com/djcass44/go-tracer v0.2.0
	github.com/djcass44/go-utils v0.2.0
	github.com/getsentry/sentry-go v0.6.1
	github.com/google/btree v1.0.0 // indirect
	github.com/google/uuid v1.1.2
	github.com/gorilla/mux v1.7.0
	github.com/kr/text v0.2.0 // indirect
	github.com/namsral/flag v1.7.4-pre
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/peterbourgon/diskv v2.0.1+incompatible
	github.com/prometheus/client_golang v1.6.0
	github.com/rs/cors v1.7.0
	github.com/sirupsen/logrus v1.7.0
	github.com/stretchr/testify v1.6.1
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f // indirect
	gopkg.in/yaml.v2 v2.3.0
)
