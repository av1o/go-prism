ARG GITLAB_PREFIX
FROM ${GITLAB_PREFIX}registry.gitlab.com/av1o/base-images/go-git:master as API_BUILDER

RUN mkdir -p /home/somebody/go && \
    mkdir -p /home/somebody/.tmp
WORKDIR /home/somebody/go

ARG GOPRIVATE

ARG AUTO_DEVOPS_GO_GIT_CFG
ARG AUTO_DEVOPS_GO_COMPILE_FLAGS

# copy our code in
COPY --chown=somebody:0 . .

# KANIKO_BUILD_ARGS=AUTO_DEVOPS_GO_GIT_CFG="url.git@github.com.insteadOf https://github.com"
RUN if [[ -n "${AUTO_DEVOPS_GO_GIT_CFG}" ]]; then git config --global $(echo "${AUTO_DEVOPS_GO_GIT_CFG}" | tr -d '"' | tr -d "'" | sed -e 's/%20/ /g'); fi

# build the binary
RUN if [ -d "./cmd" ]; then BUILD_DIR="./cmd/..."; else BUILD_DIR="."; fi && \
	TMPDIR=/home/somebody/.tmp CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -installsuffix cgo -ldflags '-extldflags "-static"' $(echo "${AUTO_DEVOPS_GO_COMPILE_FLAGS}" | tr -d '"' | tr -d "'" | sed -e 's/%20/ /g') -o main "${BUILD_DIR}"

# build the ui
FROM ${GITLAB_PREFIX}registry.gitlab.com/av1o/base-images/node:master as UI_BUILDER

# disable spammy donation messages
ENV DISABLE_OPENCOLLECTIVE=true
ENV GATSBY_TELEMETRY_DISABLED 1

RUN mkdir -p /home/somebody/build
WORKDIR /home/somebody/build

# inject auth if it's present
ARG NPM_REGISTRY
ARG	NPM_TOKEN
ARG NPM_EXTRA_ARGS
RUN if [ -n "${NPM_TOKEN:-}" ]; then npm set "//${NPM_REGISTRY:-registry.npmjs.org}:_authToken" "${NPM_TOKEN}"; fi

COPY ./web/package*.json ./
RUN npm ci --registry=https://${NPM_REGISTRY:-registry.npmjs.org} ${NPM_EXTRA_ARGS}


COPY ./web .

RUN npm run build
RUN chown -R 1001:0 /home/somebody/build/build

# runner
FROM ${GITLAB_PREFIX}registry.gitlab.com/av1o/base-images/scratch:master

COPY --from=UI_BUILDER /home/somebody/build/build /static

WORKDIR /app
# copy the compiled binary from the builder
COPY --from=API_BUILDER /home/somebody/go/main /app/
COPY --from=API_BUILDER /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY config.default.yaml /etc/prism/config.yaml

USER somebody

EXPOSE 8080
ENTRYPOINT ["/app/main"]
