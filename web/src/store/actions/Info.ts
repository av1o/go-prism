/*
 *    Copyright 2020 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

import {createAction, RSAAAction} from "redux-api-middleware";
import {BASE_URL} from "../../config/constants";
import {Info} from "../../config/types/Info";
import {InfoState} from "../reducers/Info";

export const GET_INFO = "GET_INFO";
export const GET_INFO_REQUEST = "GET_INFO_REQUEST";
export const GET_INFO_SUCCESS = "GET_INFO_SUCCESS";
export const GET_INFO_FAILURE = "GET_INFO_FAILURE";

interface GetInfoRequestActionType {
	type: typeof GET_INFO_REQUEST;
	payload: void;
}

interface GetInfoSuccessActionType {
	type: typeof GET_INFO_SUCCESS;
	payload: Info;
}

interface GetInfoFailureActionType {
	type: typeof GET_INFO_FAILURE;
	payload: Error;
}

export const getInfo = (): RSAAAction => createAction<InfoState, never, string | undefined>({
	endpoint: `${BASE_URL}/api/v1/info`,
	method: "GET",
	types: [
		GET_INFO_REQUEST,
		GET_INFO_SUCCESS,
		{
			type: GET_INFO_FAILURE,
			meta: "Failed to get instance info"
		}
	]
});

export type GetInfoActionType = GetInfoRequestActionType | GetInfoSuccessActionType | GetInfoFailureActionType;