/*
 *    Copyright 2020 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

import {PaletteOptions} from "@material-ui/core/styles/createPalette";

export const dark: PaletteOptions = {
	primary: {
		main: "#449df4",
		light: "#52abff",
		dark: "#222425"
	},
	secondary: {
		main: "#d1cec7",
		light: "#3F51B5",
		dark: "#222425"
	},
	warning: {
		main: "#cd6700",
		light: "#e69400",
		dark: "#b44400"
	},
	error: {
		main: "#e26a5f",
		light: "#ff7a59",
		dark: "#430f00"
	},
	info: {
		main: "#403294",
		light: "#9288d0",
		dark: "#222425"
	},
	success: {
		main: "#78f2b7",
		light: "#4CAF50",
		dark: "#1B5E20"
	},
	background: {
		paper: "#222425",
		default: "#1a1b1c"
	},
	action: {
		selected: "#161718",
		hover: "#161718"
	},
	type: "dark"
};