interface WindowEnv {
	API_PREFIX?: string;
}

declare global {
	interface Window {
		_env_?: WindowEnv;
	}
}

// get raw values
export const BASE_URL = window._env_?.API_PREFIX || "";