import React, {useEffect, useMemo} from "react";
import {useDispatch, useSelector} from "react-redux";
import {createMuiTheme, Grid, MuiThemeProvider, Theme} from "@material-ui/core";
import logo from "./logo.svg";
import "./App.css";
import {getInfo} from "./store/actions/Info";
import {TState} from "./store/reducers";
import {InfoState} from "./store/reducers/Info";
import Source from "./containers/Source";
import Target from "./containers/Target";
import Node from "./containers/Node";
import {dark} from "./config/palette";

const App: React.FC = (): JSX.Element => {
	// hooks
	const dispatch = useDispatch();
	const {info} = useSelector<TState, InfoState>(state => state.info);

	// local state
	const theme: Theme = useMemo(() => {
		document.documentElement.setAttribute("data-theme", "dark");
		return createMuiTheme({
			palette: dark,
			overrides: {
				MuiTooltip: {
					tooltip: {
						fontSize: "0.9rem"
					}
				}
			}
		});
	}, []);

	useEffect(() => {
		dispatch(getInfo());
	}, [dispatch]);

	return (
		<div className="App">
			<MuiThemeProvider theme={theme}>
				<header className="App-header">
					<Grid container>
						<Grid
							className="Grid-pane"
							item
							xs={4}>
							<Source/>
						</Grid>
						<Grid item xs={4}>
							<img src={logo} className="App-logo" alt="logo"/>
							{info && <Node info={info}/>}
						</Grid>
						<Grid
							className="Grid-pane"
							item
							xs={4}>
							{info && <Target remotes={info.remotes}/>}
						</Grid>
					</Grid>
				</header>
			</MuiThemeProvider>
		</div>
	);
}

export default App;
