/*
 *    Copyright 2020 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

import React, {ReactNode, useMemo} from "react";
import {ListItem, ListItemIcon, ListItemText} from "@material-ui/core";
import Icon from "@mdi/react";
import {mdiLayersOutline} from "@mdi/js";
import {GetColor} from "@tafalk/material-color-generator";
import {makeStyles} from "@material-ui/core/styles";
import {RemoteConfig} from "../config/types/Info";

const useStyles = makeStyles(() => ({
	link: {
		color: "inherit",
		textDecoration: "inherit"
	}
}));

interface RemoteProps {
	remote: RemoteConfig;
}

const Remote: React.FC<RemoteProps> = ({remote}): JSX.Element => {
	// hooks
	const classes = useStyles();

	// local state
	const icon: ReactNode = useMemo(() => {
		switch (remote.type) {
			case "maven":
				return (<img src={`${process.env.PUBLIC_URL}/maven_logo.svg`} alt="Maven logo" height={16}/>);
			case "node":
				return (<img src={`${process.env.PUBLIC_URL}/npm_logo.svg`} alt="NPM logo" height={16}/>);
			default: {
				const colour = GetColor(remote.uri, "light");
				return (<Icon path={mdiLayersOutline} size={1} color={`#${colour}`}/>);
			}

		}
	}, [remote]);

	return (
		<ListItem>
			<ListItemIcon>
				{icon}
			</ListItemIcon>
			<ListItemText
				secondary={
					<a
						className={classes.link}
						href={remote.uri}
						target="_blank"
						rel="noopener noreferrer">
						{remote.uri}
					</a>
				}
				secondaryTypographyProps={{color: "textSecondary"}}>
				{remote.name}
			</ListItemText>
		</ListItem>
	);
};
export default Remote;