# Prism

[![pipeline status](https://gitlab.com/av1o/go-prism/badges/master/pipeline.svg)](https://gitlab.com/av1o/go-prism/-/commits/master)
[![coverage report](https://gitlab.com/av1o/go-prism/badges/master/coverage.svg)](https://gitlab.com/av1o/go-prism/-/commits/master)

Prism is a simple application designed for proxying & caching multiple artifact repositories.

It is tested with the following build systems:
* Maven
* Gradle
* NPM

It supports whitelisting and blacklisting urls to specified remotes.

**Example**

Requests for packages located at /org/myprivatedomain will only be forwarded to a private self-hosted registry, while all other requests will go to Maven Central.

```yaml
maven:
  remotes:
    - uri: https://repo1.maven.org/maven2
      name: central
      blacklist:
        - ^(org.myprivatedomain).+
    - uri: https://maven.myprivatedomain.org
      name: myregistry
      whitelist:
        - ^(org.myprivatedomain).+
```

## Setup

```bash
docker build -t prism .
docker run -p 8080:8080 prism
```

## Configuration

Prism uses a config file to hold its configuration.

A default config is located at [/etc/prism/config.yaml](config.default.yaml).
This config file can be overridden by the specifying the CLI argument `-external-config` or the environment variable `EXTERNAL_CONFIG`.

config.yaml

```yaml
server:
  port: 8080 # port to run on
debug: true # control debug logging
cache:
  enabled: true # if false, cache isn't checked on request
  path: /data # path to store cached data
  methods: # list of http methods of which responses can be cached
    - GET
    - HEAD
  restrictedHeaders: # list of headers of which following requests must match
    - Authorization
checkScheme: false # check that the request scheme is http or https
maven:
  remotes: # list of acceptable maven remotes
    - uri: https://repo1.maven.org/maven2 # url of the remote (must be unique)
      name: central # friendly name
      blacklist: # list of regex to BLOCK
        - ^(org.springframework).+ # all requests to org.springframework.* will be denied to this remote
    - uri: https://jitpack.io
      name: jitpack
      whitelist: # list of regex to ALLOW
        - ^(com.github).+ # only requests to com.github.* will be allowed to this remote
npm:
  remotes: # same as maven
    - uri: https://registry.npmjs.org
      name: npmjs
      whitelist:
        - .* # allow everything (same as excluding whitelist)
``` 