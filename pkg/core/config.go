/*
 *    Copyright 2020 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package core

import (
	log "github.com/sirupsen/logrus"
	"gitlab.dcas.dev/open-source/go-prism/pkg/remote"
	"gopkg.in/yaml.v2"
	"io/ioutil"
)

type GlobalConfig struct {
	Cache        CacheConfig  `yaml:"cache"`
	MavenConfig  mavenConfig  `yaml:"maven"`
	NodeConfig   nodeConfig   `yaml:"npm"`
	AlpineConfig remoteConfig `yaml:"alpine"`
	Server       serverConfig `yaml:"server"`
	Debug        bool         `yaml:"debug"`
	CheckScheme  bool         `yaml:"checkScheme"`
}

type serverConfig struct {
	Port int `yaml:"port"`
}

type CacheConfig struct {
	Enabled           bool     `yaml:"enabled"`
	Path              string   `yaml:"path"`
	Methods           []string `yaml:"methods"`
	RestrictedHeaders []string `yaml:"restrictedHeaders"`
}

type remoteConfig struct {
	Remotes []*remote.Config `yaml:"remotes"`
}

type mavenConfig struct {
	Remotes []*remote.Config `yaml:"remotes"`
}
type nodeConfig struct {
	Remotes []*remote.Config `yaml:"remotes"`
}

// Deprecated
type RemoteConfig struct {
	URI             string   `yaml:"uri" json:"uri"`
	Name            string   `yaml:"name" json:"name"`
	StripRestricted bool     `yaml:"stripRestricted" json:"stripRestricted"`
	Blacklist       []string `yaml:"blacklist" json:"blacklist"`
	Whitelist       []string `yaml:"whitelist" json:"whitelist"`
	Type            *string  `json:"type"`
}

var Config = &GlobalConfig{}

func ReadConfig(path *string) (*GlobalConfig, error) {
	// read file data
	file, err := ioutil.ReadFile(*path)
	if err != nil {
		log.Errorf("failed to read file %s: %s", *path, err)
		return nil, err
	}
	// convert yaml to struct
	var conf = GlobalConfig{
		Cache: CacheConfig{
			Enabled: true,
		},
	}
	err = yaml.Unmarshal(file, &conf)
	if err != nil {
		log.Errorf("failed to unmarshal file %s: %s", *path, err)
		return nil, err
	}
	return &conf, nil
}
