/*
 *    Copyright 2020 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package core

import (
	"context"
	"github.com/stretchr/testify/assert"
	"gitlab.dcas.dev/open-source/go-prism/pkg/policy"
	"gitlab.dcas.dev/open-source/go-prism/pkg/remote"
	"net/http"
	"testing"
)

func TestGetProxyRequest(t *testing.T) {
	client := NewProxyClient()
	request := &http.Request{
		Method: http.MethodGet,
		Header: map[string][]string{},
	}
	request.Header.Add("Authorization", "password1")
	r := &remote.Config{
		Name:            "test",
		URI:             "example.org",
		StripRestricted: true,
	}
	p, _ := policy.NewRegexRemotePolicy(r)
	Config.Cache.RestrictedHeaders = []string{"Authorization"}
	target := "pom.xml"
	proxyRequest, err := client.getProxyRequest(context.TODO(), request, p, target, nil)
	assert.Nil(t, err)
	header := proxyRequest.Header.Get("Authorization")
	// check that the header has been removed
	assert.Empty(t, header)
}
