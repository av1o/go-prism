/*
 *    Copyright 2020 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package core

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var (
	cacheHits = promauto.NewCounter(prometheus.CounterOpts{
		Name: "prism_cache_hits_total",
		Help: "Total number of cached results returned",
	})
	cacheMiss = promauto.NewCounter(prometheus.CounterOpts{
		Name: "prism_cache_misses_total",
		Help: "Total number of cache misses",
	})
	cacheTotal = promauto.NewCounter(prometheus.CounterOpts{
		Name: "prism_cache_requests_total",
		Help: "Total number of cache requests",
	})
	cachePutErr = promauto.NewCounter(prometheus.CounterOpts{
		Name: "prism_cache_put_errs_total",
		Help: "Total number of failed cache puts",
	})
	bytesTotal = promauto.NewCounter(prometheus.CounterOpts{
		Name: "prism_net_bytes_total",
		Help: "Total number of bytes transferred",
	})
	netTotal = promauto.NewCounter(prometheus.CounterOpts{
		Name: "prism_net_total",
		Help: "Total number of network requests",
	})
	netSuccess = promauto.NewCounter(prometheus.CounterOpts{
		Name: "prism_net_success",
		Help: "Total number of successful network requests",
	})
	netFail = promauto.NewCounter(prometheus.CounterOpts{
		Name: "prism_net_failed",
		Help: "Total number of failed network requests",
	})
	remoteResponseSuccess = promauto.NewCounter(prometheus.CounterOpts{
		Name: "prism_remote_success_total",
		Help: "Total number of successful remote requests",
	})
	remoteResponseErrs = promauto.NewCounter(prometheus.CounterOpts{
		Name: "prism_remote_errs_total",
		Help: "Total number of failed remote requests",
	})
)
