/*
 *    Copyright 2020 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package core

import (
	"context"
	"crypto/tls"
	"fmt"
	"github.com/djcass44/go-tracer/tracer"
	"github.com/djcass44/go-utils/pkg/httputils"
	log "github.com/sirupsen/logrus"
	"gitlab.dcas.dev/open-source/go-prism/pkg/policy"
	"io"
	"net/http"
)

type ProxyClient struct {
	client *http.Client
}

func NewProxyClient() *ProxyClient {
	c := new(ProxyClient)
	c.client = &http.Client{
		Transport: &http.Transport{
			// ensure we respect the http proxy settings
			Proxy: http.ProxyFromEnvironment,
			// https://ssl-config.mozilla.org/#server=go&version=1.14.4&config=intermediate&guideline=5.6
			TLSClientConfig: &tls.Config{
				MinVersion: tls.VersionTLS12,
				CipherSuites: []uint16{
					tls.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
					tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
					tls.TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,
					tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
					tls.TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305,
					tls.TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305,
				},
			},
		},
	}

	tracer.Apply(c.client)

	return c
}

func (c *ProxyClient) getProxyRequest(ctx context.Context, r *http.Request, remote policy.RemotePolicy, target string, body io.Reader) (*http.Request, error) {
	requestID := tracer.GetContextId(ctx)
	request, err := http.NewRequestWithContext(ctx, r.Method, fmt.Sprintf("%s/%s", remote.GetRemote().URI, target), body)
	if err != nil {
		log.WithError(err).WithField("id", requestID).Error("failed to create request")
		return nil, fmt.Errorf("failed to create request to %s: %w", remote.GetRemote().Name, err)
	}
	// copy request headers to the new request
	CopyHeaders(request.Header, r.Header)
	// remove restricted headers (e.g. Authorization)
	if remote.GetRemote().StripRestricted {
		for _, v := range Config.Cache.RestrictedHeaders {
			request.Header.Del(v)
		}
	}
	return request, nil
}

func (c *ProxyClient) ClientRequest(ctx context.Context, r *http.Request, result chan *Result, target string, remote policy.RemotePolicy, body io.Reader) {
	requestID := tracer.GetContextId(ctx)
	request, err := c.getProxyRequest(ctx, r, remote, target, body)
	if err != nil {
		result <- &Result{
			Remote:   remote,
			Success:  false,
			Response: nil,
			Context:  ctx,
		}
		return
	}
	log.WithField("id", requestID).Debugf("making %s request to %s", request.Method, request.URL)
	response, err := c.client.Do(request)
	if err != nil {
		log.WithError(err).WithField("id", requestID).Errorf("failed to execute request to %s", remote.GetRemote().Name)
		result <- &Result{
			Remote:   remote,
			Success:  false,
			Response: nil,
			Context:  ctx,
		}
		return
	}
	if httputils.IsHTTPError(response.StatusCode) {
		log.WithField("id", requestID).Warnf("got %d (failed) response from remote %s %s", response.StatusCode, request.Method, remote.GetRemote().Name)
		result <- &Result{
			Remote:   remote,
			Success:  false,
			Response: response, // send the response for possible triage
			Context:  ctx,
		}
		return
	}
	log.WithField("id", requestID).Infof("got acceptable response from remote %s %s", request.Method, remote.GetRemote().Name)
	// put the data into the channel
	result <- &Result{
		Remote:   remote,
		Success:  true,
		Response: response,
		Context:  ctx,
	}
}
