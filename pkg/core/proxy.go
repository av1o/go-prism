/*
 *    Copyright 2020 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package core

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/djcass44/go-tracer/tracer"
	"github.com/djcass44/go-utils/pkg/sliceutils"
	"github.com/getsentry/sentry-go"
	"github.com/google/uuid"
	log "github.com/sirupsen/logrus"
	"gitlab.dcas.dev/open-source/go-prism/pkg/policy"
	"gitlab.dcas.dev/open-source/go-prism/pkg/provider"
	"gitlab.dcas.dev/open-source/go-prism/pkg/remote"
	"io/ioutil"
	"math"
	"net/http"
	"strings"
	"time"
)

type contextKey int

const ContextKeyID contextKey = iota

type DefaultProxy struct {
	Path    string
	Remotes []policy.RemotePolicy
	Config  *CacheConfig
	cache   provider.Provider
	client  *ProxyClient
}

func NewProxy(path string, remotes []*remote.Config, config *CacheConfig, cache provider.Provider) (*DefaultProxy, error) {
	policies, err := policy.NewRegexRemotePolicies(remotes)
	if err != nil {
		return nil, err
	}
	p := new(DefaultProxy)
	p.Path = path
	p.Remotes = policies
	p.Config = config
	p.cache = cache
	p.client = NewProxyClient()
	return p, nil
}

type Result struct {
	Remote   policy.RemotePolicy
	Context  context.Context
	Success  bool
	Response *http.Response
}

type ProxyRequests interface {
	Head(r chan *Result, target *string, remote string, body []byte)
}

// CopyHeaders copies headers from source to destination
func CopyHeaders(dst, src http.Header) {
	for k, vv := range src {
		for _, v := range vv {
			dst.Add(k, v)
		}
	}
}

var cacheableBodyMethods = []string{
	http.MethodGet,
}

func (p *DefaultProxy) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	requestID := tracer.GetRequestId(r)
	netTotal.Inc()
	// start the clock
	startTime := time.Now()

	target := strings.TrimPrefix(r.URL.Path, p.Path)
	log.WithField("id", requestID).Debugf("got target %s", target)

	// check the cache
	if p.Config.Enabled && sliceutils.Includes(p.Config.Methods, r.Method) {
		cacheTotal.Inc()
		err := p.returnCachedEntry(w, r, r.URL.Path)
		if err == nil {
			cacheHits.Inc()
			log.WithField("id", requestID).Infof("found cache entry for path %s", r.URL.Path)
			log.WithField("id", requestID).Infof("got cached response in %v", time.Since(startTime))
			// cache was hit, no need to do anything further
			return
		}
		log.WithField("id", requestID).Infof("could not find a cache entry for %s: %s", r.URL.Path, err)
		cacheMiss.Inc()
	}

	remotes := policy.GetMatchingRemotes(target, p.Remotes)
	size := len(remotes)
	log.WithField("id", requestID).Infof("checking %d/%d remote(s)", size, len(p.Remotes))
	result := make(chan *Result)

	defer r.Body.Close()

	ctxMap := map[string]context.CancelFunc{}
	for _, pol := range remotes {
		id := uuid.New().String()
		ctx, cancel := context.WithCancel(context.WithValue(r.Context(), ContextKeyID, id))
		ctxMap[id] = cancel
		go p.client.ClientRequest(ctx, r, result, target, pol, r.Body)
	}
	// early bird gets the worm
	count := 0
	var failure *Result
	var res *Result
	for count < size {
		res = <-result
		// if we got a good response, exit the loop
		if res.Success {
			count = math.MaxInt32 // should cover it
			remoteResponseSuccess.Inc()
			log.WithField("id", requestID).Debug("found successful result in channel")
			id := res.Context.Value(ContextKeyID).(string)
			// cancel the other contexts
			for k, v := range ctxMap {
				if k != id {
					v()
				}
			}
		} else {
			// store responses backwards so 1st is most recent
			if res.Response != nil {
				failure = res
				log.WithField("id", requestID).Debug("saving unsuccessful result in channel as fallback response")
			} else {
				log.WithField("id", requestID).Debug("found unsuccessful result in channel")
			}
			remoteResponseErrs.Inc()
		}
		count++
	}
	var response *http.Response
	var allowCache bool
	// if we got nothing, return 404
	if res == nil || !res.Success {
		netFail.Inc()
		log.WithField("id", requestID).Infof("received final fail signal with no successful responses")

		if failure == nil {
			log.WithField("id", requestID).Debugf("cannot find failed response to return, will return generic 404")
			w.WriteHeader(http.StatusNotFound)
			_, _ = w.Write([]byte("404 not found"))
			return
		}
		// we should still return the response, even though it failed
		allowCache = false
		response = failure.Response
	} else {
		// return the response AND cache it
		netSuccess.Inc()
		response = res.Response
		allowCache = sliceutils.Includes(cacheableBodyMethods, r.Method)
		if failure != nil {
			_ = failure.Response.Body.Close()
		}
	}
	canCache := true
	if res != nil {
		canCache = res.Remote.CanCache(r.URL.Path)
	}
	log.WithField("id", requestID).Infof("got response from remote in %v", time.Since(startTime))
	p.proxyResponse(w, r, response, &r.URL.Path, allowCache && canCache)
}

func (p *DefaultProxy) proxyResponse(w http.ResponseWriter, req *http.Request, r *http.Response, path *string, allowCache bool) {
	requestID := tracer.GetRequestId(req)
	// otherwise proxy the data
	log.WithField("id", requestID).Debugf("returning proxied data")

	key := *path + GetRestrictedHeaderHash(req)
	// cache the headers
	cacheHeaders := http.Header{}
	CopyHeaders(cacheHeaders, r.Header)
	headers, err := json.Marshal(cacheHeaders)
	if err == nil {
		hPath := fmt.Sprintf("%s.headers", key)
		log.WithField("id", requestID).Debugf("adding headers to cache for path %s", hPath)
		if err := p.cache.PutEntry(hPath, headers); err != nil {
			log.WithError(err).WithField("id", requestID).Error("failed to write cache entry")
			cachePutErr.Inc()
		}
	} else {
		log.WithField("id", requestID).Debugf("failed to add cache entry: %s", err)
		sentry.CaptureException(err)
	}

	// make a copy that we can put in the cache
	body, err := ioutil.ReadAll(r.Body)
	bytesTotal.Add(float64(len(body)))
	log.WithField("id", requestID).Debugf("got response body with size %d", len(body))
	// only add a cache entry if the method supports caching (e.g. not HEAD)
	if err == nil && allowCache {
		log.WithField("id", requestID).Debugf("adding entry to cache for path %s", key)
		if err := p.cache.PutEntry(key, body); err != nil {
			log.WithError(err).WithField("id", requestID).Error("failed to write cache entry")
			cachePutErr.Inc()
		}
	}
	// return data
	_ = p.returnResults(w, &r.Header, r.StatusCode, body)
}

func (p *DefaultProxy) returnResults(w http.ResponseWriter, h *http.Header, code int, body []byte) error {
	CopyHeaders(w.Header(), *h)
	w.WriteHeader(code)
	copied, err := w.Write(body)
	log.Debugf("returned %d bytes", copied)
	return err
}

func (p *DefaultProxy) returnCachedEntry(w http.ResponseWriter, r *http.Request, path string) error {
	requestID := tracer.GetRequestId(r)
	key := path + GetRestrictedHeaderHash(r)
	// check if we have an entry first
	if !p.cache.HasKey(key) {
		log.WithField("id", requestID).Debugf("key not present in cache: %s", key)
		return errors.New("key not present")
	}
	if !p.cache.HasKey(fmt.Sprintf("%s.headers", key)) {
		log.WithField("id", requestID).Debugf("header key not present in cache: %s", key)
		return errors.New("header key not present")
	}
	// load the header json
	headerData, err := p.cache.GetStreamedEntry(fmt.Sprintf("%s.headers", key))
	if err != nil {
		log.WithError(err).WithField("id", requestID).Errorf("found nil header data for path %s", key)
		return err
	}
	defer headerData.Close()
	// convert header json to actual struct
	var headers http.Header
	err = json.NewDecoder(headerData).Decode(&headers)
	if err != nil {
		log.WithError(err).WithField("id", requestID).Errorf("found nil payload data for path %s", key)
		return err
	}
	// load the file body
	bodyData, err := p.cache.GetStreamedEntry(key)
	if err != nil {
		log.WithError(err).WithField("id", requestID).Error("failed to get disk entry")
		return err
	}
	defer bodyData.Close()
	body, err := ioutil.ReadAll(bodyData)
	if err != nil {
		log.WithError(err).WithField("id", requestID).Error("failed to read disk entry")
		return err
	}
	// check that we've loaded a valid entry
	if len(body) == 0 {
		log.WithField("id", requestID).Warnf("found empty cached body %s", key)
		return errors.New("body length is 0")
	}
	// return data to the user
	return p.returnResults(w, &headers, http.StatusOK, body)
}
