/*
 *    Copyright 2020 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package api

import (
	"github.com/djcass44/go-tracer/tracer"
	"github.com/djcass44/go-utils/pkg/httputils"
	log "github.com/sirupsen/logrus"
	"net/http"
)

type V1 struct{}

func NewV1() *V1 {
	a := new(V1)
	return a
}

func (a *V1) GetHealth(w http.ResponseWriter, r *http.Request) {
	log.WithFields(log.Fields{
		"userAgent":  r.UserAgent(),
		"remoteAddr": r.RemoteAddr,
	}).Debug("answering probe")
	// returned canned ok response
	_, _ = w.Write([]byte("OK"))
}

func (a *V1) Info(w http.ResponseWriter, r *http.Request) {
	id := tracer.GetRequestId(r)
	w.Header().Set(httputils.ContentType, httputils.ApplicationJSON)
	info := RootInfo{}
	if err := info.GetInfo(); err != nil {
		log.WithError(err).WithField("id", id).Error("failed to load instance info")
		http.Error(w, "failed to load instance info", http.StatusInternalServerError)
		return
	}
	httputils.ReturnJSON(w, http.StatusOK, &info)
}
