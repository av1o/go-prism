/*
 *    Copyright 2020 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package api

import (
	"github.com/google/uuid"
	log "github.com/sirupsen/logrus"
	"gitlab.dcas.dev/open-source/go-prism/pkg/core"
	"gitlab.dcas.dev/open-source/go-prism/pkg/remote"
	"time"
)

var IStart = time.Now()
var IID = uuid.New()

type RootInfo struct {
	APIVersion    int         `json:"api_version"`
	Remotes       *RemoteInfo `json:"remotes"`
	InstanceStart int64       `json:"instance_start_time"`
	InstanceID    uuid.UUID   `json:"instance_id"`
}

type RemoteInfo struct {
	Count   int              `json:"count"`
	Remotes []*remote.Config `json:"remotes"`
}

type DiskInfo struct {
	Total uint64 `json:"total"`
	Used  uint64 `json:"used"`
	Free  uint64 `json:"free"`
}

type CacheInfo struct {
	Enabled bool   `json:"enabled"`
	Count   uint64 `json:"file_count"`
	Size    int64  `json:"size"`
	MaxSize uint64 `json:"max_size"`
}

func InitInfo() {
	log.Info("initialising /info required data")
	mType := "maven"
	nType := "node"
	for _, r := range core.Config.MavenConfig.Remotes {
		r.Type = &mType
	}
	for _, r := range core.Config.NodeConfig.Remotes {
		r.Type = &nType
	}
}

func (r *RootInfo) GetInfo() error {
	r.APIVersion = 1.0
	r.Remotes = &RemoteInfo{
		Count:   len(core.Config.MavenConfig.Remotes) + len(core.Config.NodeConfig.Remotes),
		Remotes: append(core.Config.MavenConfig.Remotes, core.Config.NodeConfig.Remotes...),
	}
	r.InstanceStart = IStart.Unix()
	r.InstanceID = IID

	return nil
}
