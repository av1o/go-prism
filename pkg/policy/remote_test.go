/*
 *    Copyright 2020 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package policy

import (
	"github.com/stretchr/testify/assert"
	"gitlab.dcas.dev/open-source/go-prism/pkg/remote"
	"testing"
)

func TestRegexRemotePolicy_CanReceiveNothing(t *testing.T) {
	r := &remote.Config{
		Name:            "test",
		URI:             "example.org",
		StripRestricted: true,
		Blacklist:       []string{".*"},
	}
	p, _ := NewRegexRemotePolicy(r)
	assert.False(t, p.CanReceive("/test/pom.xml"))
}

func TestRegexRemotePolicy_BlacklistOverWhitelist(t *testing.T) {
	r := &remote.Config{
		Name:            "test",
		URI:             "example.org",
		StripRestricted: true,
		Blacklist:       []string{".*"},
		Whitelist:       []string{".*"},
	}
	p, _ := NewRegexRemotePolicy(r)
	assert.False(t, p.CanReceive("/test/pom.xml"))
}

func testRemoteWithType(t *testing.T, remoteType, path string, expected bool) {
	r := &remote.Config{Type: &remoteType}
	p, _ := NewRegexRemotePolicy(r)
	assert.Equal(t, expected, p.CanCache(path))
}

func TestRegexRemotePolicy_CantCacheNilType(t *testing.T) {
	testRemoteWithType(t, "", "/tmp/test.jar", false)
}

func TestRegexRemotePolicy_CanCacheMavenType(t *testing.T) {
	testRemoteWithType(t, "maven", "/tmp/maven-metadata.xml", false)
	testRemoteWithType(t, "maven", "/tmp/test.jar", true)
}

func TestRegexRemotePolicy_CanCacheNodeType(t *testing.T) {
	testRemoteWithType(t, "node", "/tmp/test.jar", false)
	testRemoteWithType(t, "node", "/tmp/test.tgz", true)
}
