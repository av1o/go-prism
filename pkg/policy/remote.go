/*
 *    Copyright 2020 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package policy

import (
	log "github.com/sirupsen/logrus"
	"gitlab.dcas.dev/open-source/go-prism/pkg/remote"
	"regexp"
	"strings"
)

type RegexRemotePolicy struct {
	remote     *remote.Config
	blockRules []regexp.Regexp
	allowRules []regexp.Regexp
}

// NewRegexRemotePolicies creates a list of RegexRemotePolicy from a list of remote.Config
func NewRegexRemotePolicies(remotes []*remote.Config) ([]RemotePolicy, error) {
	policies := make([]RemotePolicy, len(remotes))
	for i, r := range remotes {
		pol, err := NewRegexRemotePolicy(r)
		// fail fast if we get any error
		if err != nil {
			return nil, err
		}
		policies[i] = pol
	}
	return policies, nil
}

// NewRegexRemotePolicy creates a RegexRemotePolicy from a given remote.Config
func NewRegexRemotePolicy(remote *remote.Config) (RemotePolicy, error) {
	rrp := new(RegexRemotePolicy)
	blocks, err := rrp.compile(remote.Blacklist)
	if err != nil {
		return nil, err
	}
	allows, err := rrp.compile(remote.Whitelist)
	if err != nil {
		return nil, err
	}
	lenB := len(blocks)
	lenA := len(allows)
	log.Infof("generated %d rule(s) with %d allowing and %d blocking for remote %s", lenB+lenA, lenA, lenB, remote.Name)
	rrp.blockRules = blocks
	rrp.allowRules = allows
	rrp.remote = remote

	return rrp, nil
}

func (rrp *RegexRemotePolicy) GetRemote() *remote.Config {
	return rrp.remote
}

// compile generates a number of RegExp structs from a list of strings
// any error during compilation will cause the func to return early
func (rrp *RegexRemotePolicy) compile(items []string) ([]regexp.Regexp, error) {
	rules := make([]regexp.Regexp, len(items))
	// compile the regexps
	for i, s := range items {
		r, err := regexp.Compile(s)
		if err != nil {
			log.WithError(err).Errorf("failed to compile regexp '%s'", s)
			return nil, err
		}
		rules[i] = *r
	}
	return rules, nil
}

// CanCache determines whether a given path is allowed to be cached
func (rrp *RegexRemotePolicy) CanCache(path string) bool {
	if rrp.remote.Type == nil || *rrp.remote.Type == "" {
		log.Warnf("remote %s has no type, caching is disabled", rrp.remote.Name)
		return false
	}
	var canCache bool
	switch *rrp.remote.Type {
	case "node":
		// check node remotes as we can only cache certain files
		canCache = strings.HasSuffix(path, "tgz")
	case "maven":
		// make sure we don't cache the metadata files
		canCache = !strings.HasSuffix(path, "maven-metadata.xml")
	case "alpine":
		// we can cache everything except the index, until we can generate our own
		canCache = !strings.HasSuffix(path, "APKINDEX.tar.gz")
	default:
		// otherwise assume we can cache
		canCache = true
	}
	log.Debugf("cache - %s - %s - %v", *rrp.remote.Type, path, canCache)
	return canCache
}

// CanReceive determines whether a given path is allowed to be proxied to the given remote
func (rrp *RegexRemotePolicy) CanReceive(path string) bool {
	// check the blacklist first
	if rrp.anyMatch(path, rrp.blockRules) {
		log.Debugf("[%s] blocked by blacklist: %s", rrp.remote.Name, path)
		return false
	}
	// check the allow list
	if len(rrp.allowRules) > 0 && !rrp.anyMatch(path, rrp.allowRules) {
		log.Debugf("[%s] blocked by whitelist: %s", rrp.remote.Name, path)
		return false
	}
	// if there's no explicit rules, then we allow
	return true
}

// anyMatch checks whether any given regexp rules match a given path
func (rrp *RegexRemotePolicy) anyMatch(path string, rules []regexp.Regexp) bool {
	// check if any regex matches the path
	for _, regex := range rules {
		if regex.MatchString(path) {
			return true
		}
	}
	return false
}
