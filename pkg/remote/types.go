/*
 *    Copyright 2020 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package remote

type Config struct {
	URI             string   `yaml:"uri" json:"uri"`
	Name            string   `yaml:"name" json:"name"`
	StripRestricted bool     `yaml:"stripRestricted" json:"stripRestricted"`
	Blacklist       []string `yaml:"blacklist" json:"blacklist"`
	Whitelist       []string `yaml:"whitelist" json:"whitelist"`
	Type            *string  `json:"type"`
}
