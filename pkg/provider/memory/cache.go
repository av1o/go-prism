/*
 *    Copyright 2020 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package memory

import (
	"bytes"
	"crypto/sha256"
	"encoding/hex"
	"errors"
	"github.com/peterbourgon/diskv"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	log "github.com/sirupsen/logrus"
	"io"
	"sync"
)

var transform = func(s string) []string {
	return []string{}
}

var (
	keyHit = promauto.NewCounter(prometheus.CounterOpts{
		Name: "prism_key_hits_total",
		Help: "Total number of cached keys",
	})
	keyMiss = promauto.NewCounter(prometheus.CounterOpts{
		Name: "prism_key_misses_total",
		Help: "Total number of missed keys",
	})
	metricPut = promauto.NewCounter(prometheus.CounterOpts{
		Name: "prism_memcache_puts_total",
		Help: "Total number of objects written to the in-memory cache",
	})
	metricPutErr = promauto.NewCounter(prometheus.CounterOpts{
		Name: "prism_memcache_puts_errs_total",
		Help: "Total number of errors writing to the in-memory cache",
	})
	metricGet = promauto.NewCounter(prometheus.CounterOpts{
		Name: "prism_memcache_gets_total",
		Help: "Total number of objects read from the in-memory cache",
	})
	metricGetErr = promauto.NewCounter(prometheus.CounterOpts{
		Name: "prism_memcache_gets_errs_total",
		Help: "Total number of errors reading from the in-memory cache",
	})
)

type Cache struct {
	Path        string
	keyMap      map[string]string
	keyMapMutex sync.RWMutex
	v           *diskv.Diskv
}

func New(path string) *Cache {
	c := new(Cache)
	c.Path = path
	c.keyMap = map[string]string{}
	c.keyMapMutex = sync.RWMutex{}
	c.initDiskCache()
	return c
}

// initDiskCache creates the underlying memory abstraction
func (c *Cache) initDiskCache() {
	c.v = diskv.New(diskv.Options{
		BasePath:     c.Path,
		Transform:    transform,
		CacheSizeMax: 1024 * 1024 * 1024 * 2, // 2 GiB
	})
	log.Infof("initialised memory at %s", c.Path)
}

/*
Generates the SHA256 hash of a given path
*/
func (c *Cache) getFileKey(path string) string {
	// check if we've cached this path first
	c.keyMapMutex.RLock()
	val, ok := c.keyMap[path]
	c.keyMapMutex.RUnlock()
	// we've found a cached key, return that
	if ok {
		keyHit.Inc()
		return val
	}
	keyMiss.Inc()
	// generate the hash
	hash := sha256.New()
	hash.Write([]byte(path))
	sha := hex.EncodeToString(hash.Sum(nil))
	// add a memory entry
	c.keyMapMutex.Lock()
	c.keyMap[path] = sha
	c.keyMapMutex.Unlock()

	return sha
}

func (c *Cache) HasKey(path string) bool {
	return c.v.Has(c.getFileKey(path))
}

func (c *Cache) GetStreamedEntry(path string) (rc io.ReadCloser, err error) {
	if rc, err = c.v.ReadStream(c.getFileKey(path), false); err != nil {
		metricGetErr.Inc()
	} else {
		metricGet.Inc()
	}
	return
}

func (c *Cache) StreamEntry(path string, data io.Reader) error {
	p := c.getFileKey(path)
	log.Debugf("saving data to key %s", p)
	if err := c.v.WriteStream(p, data, false); err != nil {
		log.WithError(err).Error("error caught when writing to memory")
		metricPutErr.Inc()
		return err
	}
	metricPut.Inc()
	return nil
}

func (c *Cache) PutEntry(path string, data []byte) error {
	if len(data) == 0 {
		metricPutErr.Inc()
		log.Warnf("rejecting memory put for empty data %s", path)
		return errors.New("given data cannot be empty")
	}
	if err := c.StreamEntry(path, bytes.NewReader(data)); err != nil {
		metricPutErr.Inc()
		return err
	}
	metricPut.Inc()
	return nil
}
