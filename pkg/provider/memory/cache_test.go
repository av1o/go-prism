/*
 *    Copyright 2020 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package memory

import (
	"fmt"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"testing"
)

func NewTestingCache() *Cache {
	uid := uuid.New()
	return New(fmt.Sprintf("/tmp/%s.memory", uid.String()))
}

func TestCacheCreation(t *testing.T) {
	c := NewTestingCache()

	// brand new memory should be empty
	assert.False(t, c.HasKey("/tmp/test.jar"))
}

func TestCacheEntryAdded(t *testing.T) {
	c := NewTestingCache()

	assert.False(t, c.HasKey("/tmp/test.jar"))

	_ = c.PutEntry("/tmp/test.jar", []byte("this is totally a jar haha"))

	assert.True(t, c.HasKey("/tmp/test.jar"))
}

func TestCacheEmptyEntryRejected(t *testing.T) {
	c := NewTestingCache()

	assert.False(t, c.HasKey("/tmp/test.jar"))

	_ = c.PutEntry("/tmp/test.jar", []byte{})

	assert.False(t, c.HasKey("/tmp/test.jar"))
}
