/*
 *    Copyright 2021 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package s3

import (
	"bytes"
	"errors"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	log "github.com/sirupsen/logrus"
	"io"
	"io/ioutil"
	"strings"
)

var (
	metricPut = promauto.NewCounter(prometheus.CounterOpts{
		Name: "prism_s3_puts_total",
		Help: "Total number of objects written to S3",
	})
	metricPutErr = promauto.NewCounter(prometheus.CounterOpts{
		Name: "prism_s3_puts_errs_total",
		Help: "Total number of errors writing to S3",
	})
	metricGet = promauto.NewCounter(prometheus.CounterOpts{
		Name: "prism_s3_gets_total",
		Help: "Total number of objects read from S3",
	})
	metricGetErr = promauto.NewCounter(prometheus.CounterOpts{
		Name: "prism_s3_gets_errs_total",
		Help: "Total number of errors reading from S3",
	})
)

type Provider struct {
	conf *Config
}

func NewProvider(conf *Config) *Provider {
	p := new(Provider)
	p.conf = conf

	log.Infof("created new s3 backend provider with config: %+v", conf)

	return p
}

// HasKey checks whether a given file exists in S3
func (p *Provider) HasKey(path string) bool {
	s3client := s3.New(p.getSession())
	hoi := &s3.HeadObjectInput{
		Bucket: aws.String(p.conf.Bucket),
		Key:    aws.String(path),
	}
	log.Infof("heading object with key %s", path)
	_, err := s3client.HeadObject(hoi)
	if err != nil {
		log.WithError(err).Error("failed to head s3 object")
		return false
	}
	return true
}

// GetStreamedEntry streams a requested file back from S3
func (p *Provider) GetStreamedEntry(path string) (io.ReadCloser, error) {
	sess := p.getSession()
	downloader := s3manager.NewDownloader(sess)

	// create a temp buffer
	buf := aws.NewWriteAtBuffer(nil)

	n, err := downloader.Download(buf, &s3.GetObjectInput{
		Bucket: aws.String(p.conf.Bucket),
		Key:    aws.String(path),
	})
	if err != nil {
		metricGetErr.Inc()
		log.WithError(err).Error("failed to download object")
		return nil, err
	}
	log.Infof("read %d bytes from object", n)
	metricGet.Inc()
	// this looks hacky
	return ioutil.NopCloser(bytes.NewReader(buf.Bytes())), nil
}

// StreamEntry uploads a given stream of data to S3
func (p *Provider) StreamEntry(path string, data io.Reader) error {
	sess := p.getSession()
	uploader := s3manager.NewUploader(sess)
	result, err := uploader.Upload(&s3manager.UploadInput{
		Bucket: aws.String(p.conf.Bucket),
		Key:    aws.String(path),
		Body:   data,
	})
	if err != nil {
		metricPutErr.Inc()
		log.WithError(err).Error("failed to upload object")
		return err
	}
	metricPut.Inc()
	log.Infof("uploaded file to %s", result.Location)
	return nil
}

// PutEntry uploads a given byte array to S3
func (p *Provider) PutEntry(path string, data []byte) error {
	if len(data) == 0 {
		metricPutErr.Inc()
		log.Warnf("rejecting memory put for empty data '%s'", path)
		return errors.New("given data cannot be empty")
	}
	metricPut.Inc()
	return p.StreamEntry(path, bytes.NewReader(data))
}

// getSession gets an S3 session with the config given to the Provider constructor
func (p *Provider) getSession() *session.Session {
	config := &aws.Config{
		S3ForcePathStyle:              aws.Bool(p.conf.PathStyle),
		Region:                        aws.String(p.conf.Region),
		CredentialsChainVerboseErrors: aws.Bool(true),
	}
	// special config for non-aws s3
	if p.conf.Endpoint != "" {
		if !strings.HasPrefix(p.conf.Endpoint, "https") {
			config.DisableSSL = aws.Bool(true)
		}
		config.Endpoint = aws.String(p.conf.Endpoint)
	}
	s, err := session.NewSession(config)
	if err != nil {
		log.WithError(err).Panic("failed to create S3 session")
	}
	log.Info("generated new s3 session")
	return s
}
